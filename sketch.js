let nextBeat = 1;
const f0 = Tone.Midi(36).toFrequency();
const easing = 0.1;
let backgroundColor = 0;
let flipCount = 0;
let lastNote = 0;
let goingUP = true;
let finished = false;
let finishedfinished = false;
const noteScale = ["C3", "D3", "E3", "G3", "A3","C4", "D4", "E4", "G4", "A4", 'C5'];
// function getFreq(n){
//     return Tone.Frequency(f0*Math.pow(Math.pow(2, 1/6), n));
// }

// const note = new Tone.Event(function(time, pitch){
//     synth.triggerAttackRelease(pitch, "16n", time);
//     console.log('AAH');
// }, "C2");

function preload(){
    FFT = new Tone.FFT(1024);
    // eq = new Tone.EQ3(10, 5, 0);
    synth = new Tone.AMSynth({
        onload: function(){
            console.log('worth a shot');
        }
    }).chain(FFT, Tone.Master);
}

function setup(){
    console.log('SETUP', this);
    const cvs = createCanvas(windowWidth, windowHeight);
    frameRate(50);

    // let note = f0;
    // for(let i=0; i < 6; i++){
    //     noteScale.push(Tone.Frequency(note));
    //     note *= (5.0/3.0);
    // }
    // console.log(noteScale);
    // mouseClicked = function() {
    //     console.log('clicked');
    // };

    //set the note to loop every half measure
    // note.set({
    //     "loop" : true,
    //     "loopEnd" : "2n"
    // });

}

function draw(){
    // let targetBackground = (lastNote/noteScale.length)*255;
    // let delta = targetBackground - backgroundColor; //go buffallo
    // backgroundColor += delta*easing;
    background(0);
    stroke(128);
    strokeWeight(4);
    text(Tone.context.currentTime.toFixed(3), 0, 20);
    stroke(255);
    line(pmouseX, pmouseY, mouseX, mouseY);
    if(Tone.context.currentTime > nextBeat && !finished){
        const lastUPVal = goingUP;
        nextBeat += 0.25;
        if(lastNote > noteScale.length-1 || lastNote < 0){
            goingUP = !goingUP;
        }
        const chance = Math.random();
        if(goingUP){
            lastNote += chance >= 0.4 ? 1 : -1;
        }else{
            lastNote += chance >= 0.7 ? 1 : -1;
        }
        if(lastNote > noteScale.length-1){
            lastNote = noteScale.length-1;
            goingUP = false;
            console.log('TOP');
        }
        else if(lastNote < 0){
            lastNote = 0;
            goingUP = true;
            console.log('BOTTOM');
            if(flipCount > 3){
                finished = true;
            }
        }
        if(lastUPVal != goingUP) {
            flipCount++;
            console.log(flipCount);
        }
        synth.triggerAttackRelease(noteScale[lastNote], chance > 0.5 ? '16n' : '4n');

    }else if(finished){
        if(!finishedfinished){
            console.log('ALL DONE, GET OUT');
            // location.href = 'http://faceofdisapproval.com/';
            finishedfinished = true
        }
    }
    if(true){
        // console.log(FFT.getValue())
        let values = FFT.getValue();
        let minValue = 0;
        values.forEach((value) => {
            if(value < minValue){
                minValue = value;
            }
        });
        let x1 = 0,
            y1 = (windowHeight/2)+(values[0]-(minValue/2))*2,
            x2 = 0,
            y2 = 0;
        values.forEach((value, index) => {
            x2 = ((index)/(values.length-1))*width;
            y2 = (windowHeight/2)+(value-(minValue/2))*2;
            line(x1, y1, x2, y2);
            x1 = x2;
            y1 = y2;
        });
    }

    // console.log(Tone.context.currentTime.toFixed(3));
    // line(0, windowHeight/2, windowWidth, windowHeight/2);
}

function mouseClicked() {
    console.log('OTHER CLICK');
    finished = false;
    finishedfinished = false;
    flipCount = 0;
    lastNote = 0;
    nextBeat = Tone.context.currentTime;
    Tone.context.resume().then(() => {
        console.log('resumed');

        // note.start(0);
        // note.stop('4n');
    });
}

function windowResized(){
    resizeCanvas(windowWidth, windowHeight);

}
